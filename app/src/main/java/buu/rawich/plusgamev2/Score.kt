package buu.rawich.plusgamev2

class Score ( var scoreCorrect: Int = 0,  var scoreInCorrect: Int = 0 ) {
    fun onCorrect () {
        scoreCorrect++
    }

    fun onInCorrect () {
       scoreInCorrect++
    }
}