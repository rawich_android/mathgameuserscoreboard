package buu.rawich.plusgamev2.play

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import buu.rawich.plusgamev2.R
import buu.rawich.plusgamev2.Score
import buu.rawich.plusgamev2.databinding.FragmentPlayGameBinding


/**
 * A simple [Fragment] subclass.
 * Use the [PlayGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlayGameFragment : Fragment() {
    lateinit var binding: FragmentPlayGameBinding
    private lateinit var playGameViewModel: PlayGameViewModel
    private lateinit var  viewModelFactory: PlayGameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentPlayGameBinding>(
            inflater,
            R.layout.fragment_play_game,
            container,
            false
        )
//        playGameViewModel = PlayGameViewModel(
//            Score(
//                PlayGameFragmentArgs.fromBundle(requireArguments()).scoreCorrect,
//                PlayGameFragmentArgs.fromBundle(requireArguments()).scoreIncorrect
//            ), PlayGameFragmentArgs.fromBundle(requireArguments()).menu,
//            PlayGameFragmentArgs.fromBundle(requireArguments()).finalScore
//        )

        viewModelFactory = PlayGameViewModelFactory(
            Score(
                PlayGameFragmentArgs.fromBundle(requireArguments()).scoreCorrect,
                PlayGameFragmentArgs.fromBundle(requireArguments()).scoreIncorrect,
            ),
            PlayGameFragmentArgs.fromBundle(requireArguments()).menu,
            PlayGameFragmentArgs.fromBundle(requireArguments()).finalScore
        )

        playGameViewModel = ViewModelProvider(this, viewModelFactory).get(PlayGameViewModel::class.java)

//        binding.playGameViewModel = playGameViewModel

        playGameViewModel.eventEndGame.observe(viewLifecycleOwner, Observer { EndGame ->
            if(EndGame) {
                val action = PlayGameFragmentDirections.actionPlayGameFragmentToScoreFragment(
                    playGameViewModel.finalScore.value!!,
                    playGameViewModel.score.value!!.scoreCorrect,
                    playGameViewModel.score.value!!.scoreInCorrect,
                    playGameViewModel.menu.value?:0,
                )
                NavHostFragment.findNavController(this).navigate(action)
            }
        })

        binding.btnSkip.setOnClickListener { playGameViewModel.onSkip() }

             // Observer for the Game finished event
        playGameViewModel.eventNext.observe(viewLifecycleOwner, Observer { eventNext->
            if (eventNext) {
                onNext()
                playGameViewModel.onNextComplete()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.findNavController()?.navigate(
                PlayGameFragmentDirections.actionPlayGameFragmentToScoreFragment(
                    playGameViewModel.finalScore.value!!,
                    playGameViewModel.score.value!!.scoreCorrect,
                    playGameViewModel.score.value!!.scoreInCorrect,
                    playGameViewModel.menu.value!!,
                )
            )
        }
        playGameViewModel.hasClicked.observe(viewLifecycleOwner, Observer { hasClicked ->
            binding.invalidateAll()
            if (hasClicked) {
                onCheckAnswer()
                playGameViewModel.onHasClickedFinished()
            }

        })
        binding.playGameViewModel = playGameViewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    /**
     * Called when the game is finished
     */
    private fun onNext () {
        view?.findNavController()?.navigate(
            PlayGameFragmentDirections.actionPlayGameFragmentToScoreFragment(
                playGameViewModel.finalScore.value!!,
                playGameViewModel.score.value!!.scoreCorrect,
                playGameViewModel.score.value!!.scoreInCorrect,
                playGameViewModel.menu.value!!,

            )
        )
    }

    private fun onCheckAnswer() {
        playGameViewModel.checkResult(playGameViewModel.finalScore.value!!)
        if (playGameViewModel.resultBoolean.value!!) {
            binding.txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        } else {
            binding.txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        }
    }
}