package buu.rawich.plusgamev2.score

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.rawich.plusgamev2.Score

class ScoreViewModel( score : Score,menu: Int , finalScore: Int) : ViewModel() {
    // The final score
    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    private val _eventPlayAgain = MutableLiveData<Boolean>()
    val eventPlayAgain: LiveData<Boolean>
        get() = _eventPlayAgain

    private val _eventMenu = MutableLiveData<Boolean>()
    val eventMenu: LiveData<Boolean>
        get() = _eventMenu

    private val _menu = MutableLiveData<Int>()
    val menu: LiveData<Int>
        get() = _menu

    private val _finalScore = MutableLiveData<Int>()
    val finalScore: LiveData<Int>
        get() = _finalScore

    init {
        Log.i("ScoreViewModel", "Final score is $score")
        _score.value = score
        _menu.value = menu
        _finalScore.value = finalScore
    }
    fun onPlayAgain () {
        _eventPlayAgain.value = true
    }
    fun onPlayAgainComplete () {
        _eventPlayAgain.value = false
    }
    fun onMenu () {
        _eventMenu.value = true
    }
    fun onMenuComplete () {
        _eventMenu.value = false
    }
}