package buu.rawich.plusgamev2.score

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.rawich.plusgamev2.R
import buu.rawich.plusgamev2.Score
import buu.rawich.plusgamev2.databinding.FragmentScoreBinding

class ScoreFragment : Fragment() {

    private lateinit var scoreViewModel: ScoreViewModel
    private lateinit var binding: FragmentScoreBinding
    private lateinit var  viewModelFactory: ScoreViewModelFactory


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_score,
            container,
            false
        )
//        scoreViewModel = ScoreViewModel(
//            Score(),
//            ScoreFragmentArgs.fromBundle(requireArguments()).menu ,
//            ScoreFragmentArgs.fromBundle(requireArguments()).finalScore
//        )
        viewModelFactory = ScoreViewModelFactory(
            Score(
                ScoreFragmentArgs.fromBundle(requireArguments()).scoreCorrect,
                ScoreFragmentArgs.fromBundle(requireArguments()).scoreIncorrect,
            ),
            ScoreFragmentArgs.fromBundle(requireArguments()).menu,
            ScoreFragmentArgs.fromBundle(requireArguments()).finalScore
        )
        scoreViewModel = ViewModelProvider(this, viewModelFactory).get(ScoreViewModel::class.java)

        //binding.scoreViewModel = scoreViewModel

        scoreViewModel.eventPlayAgain.observe(viewLifecycleOwner, Observer { eventPlayAgain ->
            if (eventPlayAgain) {
                onPlayAgain()
                scoreViewModel.onPlayAgainComplete()
            }
        })
        scoreViewModel.eventMenu.observe(viewLifecycleOwner, Observer { eventMenu ->
            if (eventMenu) {
                view?.findNavController()?.navigate(ScoreFragmentDirections.actionScoreFragmentToTitleFragment())
                scoreViewModel.onMenuComplete()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.findNavController()?.navigate(
                ScoreFragmentDirections.actionScoreFragmentToPlayGameFragment(
                    scoreViewModel.score.value?.scoreCorrect ?: 0,
                    scoreViewModel.score.value?.scoreInCorrect ?: 0,
                    scoreViewModel.finalScore.value?:0,
                    scoreViewModel.menu.value?:0,

                )
            )
        }
        binding.scoreViewModel = scoreViewModel
        binding.lifecycleOwner = this

        return binding.root
    }
    private fun onPlayAgain() {
        view?.findNavController()?.navigate(
            ScoreFragmentDirections.actionScoreFragmentToPlayGameFragment(
                scoreViewModel.score.value?.scoreCorrect ?: 0,
                scoreViewModel.score.value?.scoreInCorrect ?: 0,
                scoreViewModel.finalScore.value!!,
                scoreViewModel.menu.value?:0

            )
        )
    }

}



