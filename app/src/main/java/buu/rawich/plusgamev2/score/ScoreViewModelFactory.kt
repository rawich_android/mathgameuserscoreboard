package buu.rawich.plusgamev2.score

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.rawich.plusgamev2.Score

class ScoreViewModelFactory (private val score : Score = Score(), private val menu : Int,private val finalScore : Int): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(score, menu,finalScore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}